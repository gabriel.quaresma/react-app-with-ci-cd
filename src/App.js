import axios from 'axios';
import { setupCache } from 'axios-cache-adapter';
import { useEffect, useState } from "react";
import './App.css';

// Create `axios-cache-adapter` instance
const cache = setupCache({
  maxAge: 1000 * 10,
  clearOnStale: true,
  clearOnError: true,
})

const api = axios.create({
  adapter: cache.adapter
})


const fetcher = async url => {
  const res = await api.get(url);
  return res.data;
}

function usePosts(id) {

  const [posts, setPosts] = useState(null)

  useEffect(() => {
    fetcher(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then(result => {
        setPosts(result)
      })
  }, [id])

  return {
    posts
  }
}

function Posts() {
  const [id, setId] = useState(1)
  const { posts } = usePosts(id)

  return (
    <div className="App">
      {JSON.stringify(posts && posts)}
      <br></br>
      <button onClick={() => setId(id + 1)}>search</button>
      <button onClick={() => setId(id - 1)}>go back</button>
    </div>
  )
}

function App() {


  return (
    <Posts />
  );
}

export default App;
